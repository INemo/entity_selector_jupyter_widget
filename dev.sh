#!/usr/bin/env sh

rm -rf build
rm -rf dist
rm -rf entity_selector_jupyter_widget.egg-info
rm -rf entity_selector_jupyter_widget/__pycache__
rm -rf entity_selector_jupyter_widget/static
rm -rf js/dist
rm -rf js/node_modules
rm -rf venv

pip3 install --user virtualenv
python3 -m virtualenv -p python3 venv
source ./venv/bin/activate
pip install jupyter ipywidgets twine
python setup.py build
pip install -e .
jupyter nbextension uninstall --py entity_selector_jupyter_widget
jupyter nbextension install --py --symlink --sys-prefix entity_selector_jupyter_widget
jupyter nbextension enable --py --sys-prefix entity_selector_jupyter_widget
echo "====="
echo "Now you can start Jupyter server and import entity_selector_jupyter_widget module"
echo "-----"
echo "if you want to rebuild module run this file again"
echo "====="
