A Jupyter Widget library for selecting entities in text

Package Install
---------------

**Prerequisites**
- [node](http://nodejs.org/)

```bash
npm install --save frontend_entity_selector_jupyter_widget
```
