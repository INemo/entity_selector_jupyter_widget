const widgets = require('@jupyter-widgets/base');
const _ = require('lodash');


let EntitySelectorModel = widgets.DOMWidgetModel.extend({
    defaults: _.extend(widgets.DOMWidgetModel.prototype.defaults(), {
        _model_name: 'HelloModel',
        _view_name: 'HelloView',
        _model_module: 'frontend_entity_selector_jupyter_widget',
        _view_module: 'frontend_entity_selector_jupyter_widget',
        _model_module_version: '0.0.1',
        _view_module_version: '0.0.1',
        search: '',
        text: '',
        res: ''
    })
});


let EntitySelectorView = widgets.DOMWidgetView.extend({
    render() {
        this.selected = '';

        this.text_select = document.createElement('p');
        this.text_select.appendChild(
            document.createTextNode(
                'Select ' + this.model.get('search')
            )
        );
        this.text_from = document.createElement('p');
        this.text_from.appendChild(
            document.createTextNode(
                'From ' + this.model.get('text')
            )
        );

        this.text_res = document.createElement('p');

        this.button_check = document.createElement('button');
        this.button_check.type = 'button';
        // this.button_check.class = 'btn';
        // this.button_check.classList.add('p-Widget jupyter-widgets jupyter-button widget-button');
        this.button_check.style.marginRight = '10px';
        this.button_check.onclick = () => {
            let selected;
            if (window.getSelection) {
                selected = window.getSelection().toString();
            } else if (document.selection && document.selection.type !== "Control") {
                selected = document.selection.createRange().text;
            }
            while (this.text_res.hasChildNodes()) {
                this.text_res.removeChild(this.text_res.firstChild);
            }
            if (selected !== '') {
                this.text_res.appendChild(
                    document.createTextNode(
                        "You selected " + selected
                    )
                );
                this.selected = selected;
            } else {
                this.text_res.appendChild(
                    document.createTextNode("You haven't selected anything")
                );
                this.selected = '';
            }
            this.model.set('res', '{"' + this.model.get('search') + '": "' + this.selected + '"}');
            this.model.save();
        };
        this.button_check.appendChild(document.createTextNode('Check'));

        // this.button_verify = document.createElement('button');
        // this.button_verify.type = 'button';
        // this.button_verify.onclick = () => {
        //     if (this.selected !== '') {
        //         this.model.set('value', this.selected);
        this.model.save_changes();
        // this.model.save();
        // }
        // };
        // this.button_verify.appendChild(document.createTextNode('Ok'));

        this.el.appendChild(this.text_select);
        this.el.appendChild(this.text_from);
        this.el.appendChild(this.text_res);
        this.el.appendChild(this.button_check);
        // this.el.appendChild(this.button_verify);
    }
});


module.exports = {
    EntitySelectorModel,
    EntitySelectorView
};